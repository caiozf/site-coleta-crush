<?php include "functions/functions.php"; 

$emails = buscaEmail($conexao);
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Lista de contatos</title>
		<meta charset="utf-8">
		<meta name="viewport" content="user-scalable=no, width=device-width">
		<link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/vendor/ress.css">
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/admin.css">

	</head>

	<body>
		<div class="container">
			
		<?php if(usuarioEstaLogado()){ ?>
			<div class="lista-contato">
				
				<div class="col-sm-12 text-center">
					<p class="usuario-logado">Você está logado como: <strong><?php echo usuarioLogado();?></strong></p>
					<a href="functions/logout.php" class="logout">Efetuar logout</a>
				</div>
				
				<div class="margin text-center">
				<button type="button" class="change-fade">Edição de textos</button>
				</div>

				<div class="hidden fade">
					<div class="col-sm-12 margin">
						<h2 class="section-title text-center">Dados do site</h2>

						<form class="form text-center" id="form-lista" action=" " method="post">
							<label for="rua">
								<p class="margin">Rua</p>
								<input class="input" type="text" name="rua" placeholder="Rua" required>
							</label>

							<label for="bairro">
								<p class="margin">Bairro</p>
								<input class="input" type="text" name="bairro" placeholder="Bairro" required>
							</label>

							<label for="cidade">
								<p class="margin">Cidade</p>
								<input class="input" type="text" name="cidade" placeholder="Cidade" required>
							</label>
							
							<label for="facebook">
								<p class="margin">Facebook</p>
								<input class="input" type="text" name="facebook" placeholder="Facebook" required>
							</label>

							<label for="instagram">
								<p class="margin">Instagram</p>
								<input class="input" type="text" name="instagram" placeholder="Instagram" required>
							</label>

							<label for="twitter">
								<p class="margin">Twitter</p>
								<input class="input" type="text" name="twitter" placeholder="Twitter" required>
							</label>

							<label for="descricao">
								<p class="margin">Texto da introdução</p>
								<textarea class="input" type="text" name="descricao" placeholder="Texto" required></textarea>
							</label>


							<button class="send">Enviar</button>
						</form>	
					</div>
				</div>
				
				<h2 class="section-title text-center">Lista de contatos</h2>
				
				<?php foreach($emails as $email) { ?>
				<div class="row striped">
					<div class="col-sm-12 col-md-3">
						<p><strong>Nome:</strong></p>
						<p><?php echo $email['nome']; ?></p>
					</div>

					<div class="col-sm-12 col-md-3">
						<p><strong>E-mail/Telefone</strong></p>
						<p><?php echo $email['email']; ?></p>
						<p><?php echo $email['telefone']; ?></p>
					</div>

					<div class="col-sm-12 col-md-3">
						<p><strong>Mensagem:</strong></p>
						<p><?php echo $email['mensagem']; ?></p>
					</div>

					<div class="col-sm-12 col-md-3">
						<a class="logout" href="functions/remove-email.php?id=<?php echo $email['id']?>">Remover</a>
					</div>
				</div>
					
				<?php } ?>	

			</div>
			
			<div class="modal">
				<div class="modal-content">
					<span class="modal-close"></span>
					<p class="modal-texto"></p>
				</div>
			</div>

			<script src="assets/js/vendor/jquery-3.2.0.min.js"></script>
			
			<script>
				var hidden = document.querySelector('.hidden');
				var chageFade = document.querySelector('.change-fade');
				chageFade.addEventListener('click', function(){
					hidden.classList.toggle('hidden');
				});

				var modalClose = document.querySelector('.modal-close');
				modalClose.addEventListener('click', function(){
					var modal = document.querySelector('.modal');
					modal.classList.remove('active');
				});

			 	$('#form-lista').submit(function(){
		     		var dados = $(this).serialize();

		     		$.ajax({
		     			type: "POST",
		     			url: "functions/dados-site.php",
		     			data: dados,
		     			success: function(data){
		     				console.log(data);
		     				$('.modal-texto').text('Textos editados com sucesso!');
		     				$('.modal').addClass('active');
		     				$('.input').each(function(){
		     					$(this).val('');
		     				});
		     			},
		     			error: function(data){
		     				console.log(data);
		     				$('.modal-texto').text('Falha no envio da mensagem!');
		     				$('.modal-content').addClass('falha');
		     				$('.modal').addClass('active');
		     			}
		     		});
		     		return false;
		     	});
			</script>

		<?php } else{ ?>
			<div class="col-sm-12">
				<div class="login text-center">
					<h2>Área restrita, efetue login</h2>
					<?php if(isset($_SESSION['danger'])){?>
					<p class="error">Confira seus dados!</p>
					<?php } ?>
					<form class="form" action="functions/login.php" method="post">
						<label for="nome">
							<p class="margin">Nome de usuário</p>
							<input class="input" type="text" name="nome" placeholder="Login" required>
						</label>

						<label for="senha">
							<p class="margin">Senha</p>
							<input class="input" type="password" name="senha" placeholder="Sua senha" required>
						</label>

						<button class="send">Efetuar Login</button>
					</form>	
				</div>
			</div>
		<?php } ?>
		</div>
	</body>
</html>