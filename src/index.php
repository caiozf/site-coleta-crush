<?php include "functions/functions.php"; 
$dados = buscaTextos($conexao);
?>

<!DOCTYPE html>

<html>
	<head>
		<title>Coleta Crush</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width">
		<link rel="stylesheet" href="assets/css/vendor/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/vendor/twentytwenty.css">
		<link rel="stylesheet" href="assets/css/vendor/animate.css">
		<link rel="stylesheet" href="assets/css/video.css">
		<link rel="stylesheet" href="assets/css/sprite.css">
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/responsive.css">

		<!-- SEO meta tags -->
		<meta name="description" content="Coleta Crush, aprenda as cores da reciclagem se divertindo!" />
		
		<meta name="author" content="Caio Zambonato Ferreira" />
		<!-- Facebook meta tags -->

		<meta property="og:title" content="Coleta Crush"/>
		<meta property="og:site_name" content="Coleta Crush"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="Coleta Crush, aprenda as cores da reciclagem se divertindo!" />

		<!-- favicons -->
		<link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicons/favicon-16x16.png">
		<link rel="manifest" href="assets/img/favicons/manifest.json">
		<link rel="mask-icon" href="assets/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="theme-color" content="#ffffff">
	</head>

	<body>
		<header class="header">
		<nav class="menu">
			<div class="container">
				<span class="burger"></span>
				<ul class="flex">
					<div class="flex-esquerda">
						<li><a class="btn" href="#sobre">Sobre</a></li>
						<li><a class="btn" href="#downloads">Para baixar</a></li>
					</div>
					

					<span class="brand">
						<h2 class="logo-text">Coleta Crush</h2>
					</span>

					<div class="flex-direita">
						<li><a class="btn" href="#section-mapa">Postos de coleta</a></li>
						<li><a class="btn" href="#contato">Contato</a></li>
					</div>
				</ul>
			</div>
		</nav>
		<img src="assets/img/logo-coleta-crush.png" alt="Coleta Crush" class="wow fadeIn header-logo">
		<span class="icon-nuvem-1 hide-mobile"></span>
		<span class="icon-nuvem-2 hide-mobile"></span>
		</header>		
		
		<section class="section verde" id="sobre">
			<div class="container">
				<h2 class="section-title text-center">Aprenda as cores da reciclagem se divertindo!</h2>

				<div class="col-sm-12 col-md-8 col-md-offset-2">
					<p><?php echo $dados['descricao']; ?></p>

					<div class="margin">
						<div class="intro-icons">
							<figure class="icon-papel"></figure>
						</div>
						
						<div class="intro-icons">
							<figure class="icon-plastico"></figure>
						</div>
						
						<div class="intro-icons">
							<figure class="icon-vidro"></figure>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="section azul inverse">
			<div class="container">
				<h2 class="section-title text-center">Ajude o Coletinha a prezervar a natureza!</h2>
				
				<div class="col-sm-12 col-md-8 col-md-offset-2">	
					<div id="container1">
					 <img src="assets/img/natureza-suja.jpg" />
					 <img src="assets/img/natureza-limpa.jpg" />
					</div>
				</div>
			</div>
		</section>

		<section class="section verde" id="downloads">
			<div class="container">
				<h2 class="section-title text-center">Para baixar</h2>
				
				<div class="row">
					<h3 class="subtitle text-center">Faça download de um dos livrinhos abaixo para se divertir !</h3>
				</div>

				<div class="row">
					<div class="col-sm-12 col-md-6">
						<div class="flex-center sm-menos-altura">
						<p class="text-center subtitle bold after plus">Passatempos</p>
						<p>Clique no link ao lado para acessar</p>
						</div>
					</div>

					<div class="col-sm-12 col-md-6">
						<div class="flex-center">
						<a style="background-image: url('assets/img/passatempos-1.jpg');" href="data/passatempos.pdf" target="_blank" class="download-btn wow fadeIn"></a>
						</div>
					</div>
				</div>

				<div class="row">

					<div class="col-sm-12 col-md-6 empurra-direita">
						<div class="flex-center sm-menos-altura">
						<p class="text-center subtitle bold after plus">Revistinha</p>
						<p>Clique no link ao lado para acessar</p>
						</div>
					</div>

					<div class="col-sm-12 col-md-6">
						<div class="flex-center">
						<a style="background-image: url('assets/img/passatempos-2.jpg');" href="data/Revistinha.pdf" target="_blank" class="download-btn wow fadeIn"></a>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12 col-md-6">
						<div class="flex-center sm-menos-altura">
						<p class="text-center subtitle bold after plus">Para colorir</p>
						<p>Clique no link ao lado para acessar</p>
						</div>
					</div>

					<div class="col-sm-12 col-md-6">
						<div class="flex-center">
						<a style="background-image: url('assets/img/passatempos-3.jpg');" href="data/para-colorir.pdf" target="_blank" class="download-btn wow fadeIn"></a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="section" id="section-mapa">
			<div class="container">
			<h2 class="section-title text-center">Postos de coleta</h2>
			<div class="row">
				<div class="col-sm-12">
					<div id="mapa">
					</div>
				</div>
			</div>
			</div>
		</section>

		<section class="section azul inverse" id="contato">
			<div class="container">
			<h2 class="section-title text-center">Fale conosco</h2>
			<form class="text-center form" action=" " method="post" id="form-contato">
				<label for="nome">
					<input class="input" type="text" id="nome" name="nome" placeholder="Nome" required>
				</label>

				<label for="email">
					<input class="input" type="email" id="email" name="email" placeholder="E-mail" required>
				</label>

				<label for="telefone">
					<input class="input" type="text" id="telefone" name="telefone" placeholder="Telefone" required>
				</label>

				<label for="mensagem">
					<textarea class="input textarea" type="text" id="mensagem" name="mensagem" placeholder="Mensagem" required></textarea>
				</label>

				<button class="send" type="submit">Enviar</button>
			</form>
			</div>
		</section>

		<section class="video hide-mobile">
		<div class="homepage-hero-module">
		    <div class="video-container">
		        <div class="filter"></div>
		        <video autoplay loop class="fillWidth">
		            <source src="assets/video/Flying-Birds.mp4" type="video/mp4" />Seu browser não suporta a tag video, recomendamos que atualize o seu navegador.
		            <source src="assets/video/Flying-Birds.webm" type="video/webm" />Seu browser não suporta a tag video, recomendamos que atualize o seu navegador.
		        </video>
		        <div class="poster hidden">
		            <img src="assets/img/Flying-Birds.jpg" alt="Pássaros voando">
		        </div>
		    </div>
		</div>
		</section>
		

		<footer class="section footer verde">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-4 text-center">
				<p class="uppercase">Endereço</p>
				<ul>
					<li>
						<p><?php echo $dados['rua']; ?></p>
					</li>
					<li>
						<p><?php echo $dados['bairro']; ?></p>
					</li>
					<li>
						<p><?php echo $dados['cidade']; ?></p>
					</li>
				</ul>
				</div>

				<div class="col-sm-12 col-md-4">
					<ul class="social">
					<p class="uppercase">Redes sociais</p>
					
					<li class="social-icon">
						<a href="<?php echo $dados['facebook']; ?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
					</li>
					
					<li class="social-icon">
						<a href="<?php echo $dados['instagram']; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
					</li>
					
					<li class="social-icon">
						<a href="<?php echo $dados['twitter']; ?>" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
					</li>
					
					</ul>
				</div>

				<div class="col-sm-12 col-md-4">
					<div class="icon-logo-ifsul"></div>
					<div class="icon-logo-sanep"></div>
				</div>
			</div>
		</div>


		</footer>

		<div class="section copyright verde inverse">
		<p>Site desenvolvido por Caio Zambonato, Trabalho acadêmico</p>
		</div>
		
		<div class="modal">
			<div class="modal-content">
				<span class="modal-close"></span>
				<p class="modal-texto"></p>
			</div>
		</div>

		<script src="assets/js/vendor/jquery-3.2.0.min.js"></script>
		<script src="assets/js/vendor/jquery.maskedinput.min.js"></script>
		<script src="assets/js/vendor/jquery.event.move.js"></script>
		<script src="assets/js/vendor/jquery.twentytwenty.js"></script>
		<script src="assets/js/video.js"></script>
		<script src="assets/js/vendor/wow.min.js"></script>
		<script src="assets/js/scripts.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChZFMgyXI9EzX8qGbTENkRLE8eL55dVM8"></script>
		<script src="assets/js/maps.js"></script>

		<script>
			new WOW().init();
			
			$('#form-contato').submit(function(){
	     		var dados = $(this).serialize();

	     		$.ajax({
	     			type: "POST",
	     			url: "email-insert.php",
	     			data: dados,
	     			success: function(data){
		     			var botao = document.querySelector('.send');
	     				console.log(data);
	     				$('.modal-texto').text('Mensagem enviada com sucesso!');
	     				$('.modal').addClass('active');
	     				$('.input').each(function(){
		     				$(this).val('');
		     			});
						botao.setAttribute('disabled', 'disabled');
	     			},
	     			error: function(data){
	     				console.log(data);
	     				$('.modal-texto').text('Falha no envio da mensagem!');
	     				$('.modal-content').addClass('falha');
	     				$('.modal').addClass('active');
	     			}
	     		});
	     		return false;
	     	});
		</script>
	</body>
</html>