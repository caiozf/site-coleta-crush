<?php include_once "functions.php";

$nome = mysqli_real_escape_string($conexao, $_POST["nome"]);
$senha = mysqli_real_escape_string($conexao, $_POST["senha"]);

$usuario = buscaUsuario($conexao, $nome, $senha);

if($usuario==null){
	$_SESSION['danger'] = "Usuário ou senha inválida!";
	header("Location: {$_SERVER['HTTP_REFERER']}");
} else{
	$_SESSION['success'] = "Usuário logado com sucesso!";
	logaUsuario($usuario["nome"]);
	header("Location: {$_SERVER['HTTP_REFERER']}");
}
die();