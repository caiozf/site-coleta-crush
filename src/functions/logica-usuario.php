<?php 
session_start();

function usuarioEstaLogado(){
	return isset($_SESSION["usuario_logado"]);
}

function usuarioLogado() {
    return $_SESSION["usuario_logado"];
}

function verificaUsuario (){
	  	if(!usuarioEstaLogado()){
	  	$_SESSION['danger'] ="Você não tem acesso a esta funcionalidade!";	
	  	die();
	  }
	}

function logaUsuario($nome){
	$_SESSION["usuario_logado"] = $nome;
}	

function logout(){
	session_destroy();
	session_start();
}

