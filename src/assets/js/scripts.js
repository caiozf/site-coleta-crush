$(document).ready(function(){

	var modalClose = document.querySelector('.modal-close');
	modalClose.addEventListener('click', function(){
		var modal = document.querySelector('.modal');
		modal.classList.remove('active');
	});

	var responsiveActive = document.querySelector('.burger');
	responsiveActive.addEventListener('click', function(){
		this.classList.toggle('open');
		var responsiveMenu = document.querySelector('.flex');
		responsiveMenu.classList.toggle('active');
	});


	$(window).scroll(function(){
		var scrollTop = $(this).scrollTop(),
			menu = $('.menu');
		if(scrollTop > 200){
			menu.addClass('scrolled');
		}else{
			menu.removeClass('scrolled');
		}
	});

	$('a[href^="#"]').click(function(e){
		e.preventDefault();
		var link = $(this).attr('href'),
			scroll = $(link).offset().top,
			menuHeight = $('.menu').innerHeight();

		$('html body').animate({
			scrollTop: scroll - menuHeight
		}, 500);
	});

	$('.logo-text').click(function(){
		$('html body').animate({
			scrollTop: 0
		}, 500)
	});

  	$("#container1").twentytwenty({
	    no_overlay: true 
  	});

  	$('#telefone').mask('(99)9999-9999?9');

  	 $('.send').click(function(){

      var input = $('.input');

      $(input).each(function(){
        if($(this).val() == '' || $(this).val() == null){
          $('.form').addClass('js-form-invalid');
        }
      }); 
    });
});