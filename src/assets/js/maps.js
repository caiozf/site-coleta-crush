/* MAPA */

    	var mapOptions = {
		    center: new google.maps.LatLng(-31.766985, -52.351304),
		    zoom: 16,
		    disableDefaultUI: true,
		    scrollwheel:  false,
		    mapTypeId: google.maps.MapTypeId.ROADMAP,
		    styles: [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    }
]
		};

		var map = new google.maps.Map(document.getElementById('mapa'), mapOptions);

		var lugares = [
			['IFSul', -31.766985, -52.351304, 1, "assets/img/markers/marker-icon.png"],
			['Sanep',-31.763738, -52.357168, 2, "assets/img/markers/marker2.png"],
			['CEEE',-31.767268, -52.348420, 3, "assets/img/markers/marker2.png"]
		];

		var markerOptions = {
		    position: new google.maps.LatLng(-31.766985, -52.351304),
		    map: map,
            scrollwheel: false,
            icon: 'assets/img/marker-icon.png'
		};

		var marker, i;
		var infowindow = new google.maps.InfoWindow();

		for(i=0; i<lugares.length; i++){
			  marker = new google.maps.Marker({
			        position: new google.maps.LatLng(lugares[i][1], lugares[i][2]),
			        icon: lugares[i][4],
			        map: map,
			        center: lugares[1]
			    });

			    google.maps.event.addListener(marker, 'click', (function (marker, i) {
			        return function () {
			            infowindow.setContent(lugares[i][0]);
			            infowindow.open(map, marker);
			        }
			    })(marker, i));
		}