-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 25-Jun-2017 às 05:36
-- Versão do servidor: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coleta-site`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `x_contato`
--

CREATE TABLE `x_contato` (
  `id` int(10) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(10) NOT NULL,
  `datetime` datetime NOT NULL,
  `telefone` varchar(255) NOT NULL,
  `mensagem` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `x_dados`
--

CREATE TABLE `x_dados` (
  `id` int(10) NOT NULL,
  `descricao` longtext NOT NULL,
  `rua` varchar(255) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `x_dados`
--

INSERT INTO `x_dados` (`id`, `descricao`, `rua`, `bairro`, `cidade`, `facebook`, `instagram`, `twitter`) VALUES
(6, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras interdum cursus lectus, in elementum risus sodales nec. Pellentesque posuere ac ante non ultrices. Sed laoreet scelerisque auctor', 'Praça 20 de Setembro, 455', 'Centro', 'Pelotas - RS', 'https://pt-br.facebook.com/IFSuloficial/', 'https://www.instagram.com/ifsul_oficial/?hl=pt-br', 'https://twitter.com/ifsulpelotas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `x_usuario`
--

CREATE TABLE `x_usuario` (
  `id` int(10) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `senha` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `x_usuario`
--

INSERT INTO `x_usuario` (`id`, `nome`, `senha`) VALUES
(2, 'administrador', '8c2429c559075b2c794d533d51671ca7');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `x_contato`
--
ALTER TABLE `x_contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `x_dados`
--
ALTER TABLE `x_dados`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `x_usuario`
--
ALTER TABLE `x_usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `x_contato`
--
ALTER TABLE `x_contato`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `x_dados`
--
ALTER TABLE `x_dados`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `x_usuario`
--
ALTER TABLE `x_usuario`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
